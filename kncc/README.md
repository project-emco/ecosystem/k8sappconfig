```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2022 Intel Corporation
```

# Kubernetes Native Configuration Controller (KNCC):

* The Controller provides support for modifying the configmap resources in an incremental manner and in the kubernetes native way, by using the Custom Resources.

* Custom Resource Definitions (CRDs) provide the ability to define the configmap changes.

* The controller is built using the golang.

# Supported Features:

* Add, Update, Delete the configmap keys and their values using the Custom Resources.

* Support for modifying the multi-line string values in the configmap.

# Documentation:

* Refer to the docs folder for the CRD documentation.

**Note** The KNCC is in active development and the Custom Resource Definitions can change in the future releases.
