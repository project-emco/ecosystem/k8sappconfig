```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2022 Intel Corporation
```

# Kubernetes Native REST Controller (KNRC) Deployment Files:

This folder has the deployment that can be used to deploy the KNRC in the kubernetes clusters.

To deploy the KNRC run the following command in the k8s control plane node,

```
make restcontrol.yaml

```

