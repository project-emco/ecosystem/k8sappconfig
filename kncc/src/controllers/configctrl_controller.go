/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/go-logr/logr"
	"gopkg.in/yaml.v3"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	kncck8sappconfigcomv1alpha1 "gitlab.com/project-emco/ecosystem/k8sappconfig/kncc/api/v1alpha1"
)

// ConfigCtrlReconciler reconciles a ConfigCtrl object
type ConfigCtrlReconciler struct {
	client.Client
	Log      logr.Logger
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
}

var (
	myFinalizerName = "restcr.lfn.edge-5g.com/finalizer"
	debug           = 0
)

func convert_cfg(x interface{}) interface{} {
	switch it := x.(type) {
	case map[interface{}]interface{}:
		mp := map[string]interface{}{}
		for k, v := range it {
			mp[k.(string)] = convert_cfg(v)
		}
		return mp
	case []interface{}:
		for x, v := range it {
			it[x] = convert_cfg(v)
		}
	}
	return x
}

func (r *ConfigCtrlReconciler) convert_mlstring(ml string) (interface{}, error) {
	var data interface{}
	if err := yaml.Unmarshal([]byte(ml), &data); err != nil {
		r.Log.Error(err, "failed to unmarshal")
		return data, err
	} else {
		data = convert_cfg(data)
		if _, err := yaml.Marshal(data); err != nil {
			r.Log.Error(err, "failed to marshal")
			return data, err
		} else {
			if debug > 0 {
				fmt.Printf("multilinedata : %v\n", data)
			}
		}
		return data, nil
	}
}

func slice_filter(input, check reflect.Value) reflect.Value {
	nsl := reflect.MakeSlice(input.Type(), 0, input.Len())
	for j := 0; j < input.Len(); j++ {
		var equal bool
		for i := 0; i < check.Len(); i++ {
			equal = reflect.DeepEqual(check.Index(i).Interface(), input.Index(j).Interface())
			if equal == true {
				break
			}
		}
		if equal == false {
			nsl = reflect.Append(nsl, input.Index(j).Elem())
		}

	}
	return nsl

}

func (r *ConfigCtrlReconciler) multiline_update(key interface{}, cfgdata interface{}, nl interface{}, del bool) (interface{}, bool) {
	x := reflect.ValueOf(key)
	switch x.Kind() {
	case reflect.String:
		imap := reflect.ValueOf(cfgdata)
		switch imap.Kind() {
		case reflect.Map:
			mapInt := imap.MapIndex(x).Elem()
			switch mapInt.Kind() {
			case reflect.Slice:
				newsl := reflect.ValueOf(reflect.ValueOf(nl).Interface())
				if newsl.Kind() != reflect.Slice {
					fmt.Printf("CR input mismatch... Slice Value expected...\n")
					return nil, false
				}
				if del == false {
					nsl := slice_filter(newsl, mapInt)
					if nsl.Len() > 0 {
						sl := reflect.AppendSlice(mapInt, nsl)
						imap.SetMapIndex(x, sl)
						return imap.Interface(), true
					}
				} else {
					nsl := slice_filter(mapInt, newsl)
					if nsl.Len() != mapInt.Len() {
						imap.SetMapIndex(x, nsl)
						return imap.Interface(), true
					}
				}
			case reflect.String:
				newsl := reflect.ValueOf(reflect.ValueOf(nl).Interface())
				if newsl.Kind() != reflect.String {
					fmt.Printf("CR input mismatch, Expected String\n")
					return nil, false
				}
				if del == false {
					imap.SetMapIndex(x, newsl)
				} else {
					imap.SetMapIndex(x, reflect.ValueOf(nil))
				}
				return imap.Interface(), true

			}
		case reflect.Slice:
			newsl := reflect.ValueOf(nl)
			if del == false {
				nsl := slice_filter(newsl, imap)
				if nsl.Len() > 0 {
					ns2 := reflect.AppendSlice(imap, nsl)
					return ns2.Interface(), true
				}
			} else {
				nsl := slice_filter(imap, newsl)
				if nsl.Len() != imap.Len() {
					return nsl.Interface(), true
				}
			}
		default:
			fmt.Printf("imap kind : %v\n imap: %v\n", imap.Kind(), imap)
			err := errors.New("Unhandled cfgdata kind")
			r.Log.Error(err, "Failed to update")
			return nil, false

		}
	case reflect.Map:
		iter := x.MapRange()
		for iter.Next() {
			k := iter.Key()
			v := iter.Value()
			if v.IsNil() {
				return r.multiline_update(k.Interface(), cfgdata, nl, del)
			} else {
				imap := reflect.ValueOf(cfgdata)
				mapInt := imap.MapIndex(k).Elem()
				found := true
				if v.Elem().Kind() == reflect.String {
					found = false
					for _, ks := range mapInt.MapKeys() {
						if reflect.DeepEqual(ks.Interface(), v.Interface()) {
							found = true
							break
						}
					}
				}
				if found == false {
					fmt.Printf("Map is Nil... Creating..\n")
					newsl := reflect.ValueOf(nl)
					mapInt.SetMapIndex(reflect.ValueOf(v.Interface()), newsl)
					imap.SetMapIndex(k, mapInt)
					return imap.Interface(), true
				} else {
					lcfg, _ := r.multiline_update(v.Interface(), mapInt.Interface(), nl, del)
					imap.SetMapIndex(k, reflect.ValueOf(lcfg))
					return imap.Interface(), true
				}
			}
		}
	}
	return nil, false
}

func (r *ConfigCtrlReconciler) reconcileDelete(ctx context.Context, cfgCrp *kncck8sappconfigcomv1alpha1.ConfigCtrl, cfg *corev1.ConfigMap, upd bool) error {
	if cfgCrp.Status.Result == "Success" && upd == true {
		if cfgerr := r.Client.Update(ctx, cfg); cfgerr != nil {
			r.Log.Error(cfgerr, "Failed to update configmap")
		}
	}

	// The object is being deleted
	if controllerutil.ContainsFinalizer(cfgCrp, myFinalizerName) {
		// remove our finalizer from the list and update it.
		controllerutil.RemoveFinalizer(cfgCrp, myFinalizerName)
		if err := r.Update(ctx, cfgCrp); err != nil {
			return err
		}
	}
	return nil
}

//+kubebuilder:rbac:groups=kncc.k8sappconfig.com.gitlab.com,resources=configctrls,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kncc.k8sappconfig.com.gitlab.com,resources=configctrls/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=kncc.k8sappconfig.com.gitlab.com,resources=configctrls/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=configmap,verbs=get;patch;watch;update;list
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.11.2/pkg/reconcile
func (r *ConfigCtrlReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("CfgCRService", req.NamespacedName)
	var cfgCr kncck8sappconfigcomv1alpha1.ConfigCtrl

	log.V(1).Info("Entering Reconcile....")
	if err := r.Get(ctx, req.NamespacedName, &cfgCr); err != nil {
		log.Error(err, "unable to fetch cfg control CR")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	var deleteInProgress bool
	update := false
	cfgMap := &corev1.ConfigMap{}
	cfgErr := r.Client.Get(ctx, types.NamespacedName{Namespace: cfgCr.Spec.Resource.NameSpace, Name: cfgCr.Spec.Resource.Name}, cfgMap)
	if cfgCr.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,                                                                      // then lets add the finalizer and update the object.
		if !controllerutil.ContainsFinalizer(&cfgCr, myFinalizerName) {
			controllerutil.AddFinalizer(&cfgCr, myFinalizerName)
			if err := r.Update(ctx, &cfgCr); err != nil {
				log.Error(err, "failed to update the finalizer")
				return ctrl.Result{}, err
			}
		}
		if cfgErr != nil {
			// Configmap not available, will retry later
			log.Error(cfgErr, "Unable to fetch the configmap")
			return ctrl.Result{RequeueAfter: 30 * time.Second}, nil
		}

		deleteInProgress = false
	} else {
		if cfgErr != nil {
			// Deleting the CR directly as the configmap is not available
			if err := r.reconcileDelete(ctx, &cfgCr, cfgMap, update); err != nil {
				log.Error(err, "Failed during deletion")
				return ctrl.Result{}, err
			}
			return ctrl.Result{}, nil
		}
		deleteInProgress = true
	}

	for _, patch := range cfgCr.Spec.Patch {
		if crdata, err := r.convert_mlstring(patch.Key); err != nil {
			log.Error(err, "cannot convert multiline patch key")
			return ctrl.Result{}, err
		} else {
			fmt.Printf("type: %v %v\n", reflect.ValueOf(crdata), reflect.ValueOf(crdata).Kind())
			switch reflect.ValueOf(crdata).Kind() {
			case reflect.Map:
				crpatch := crdata.(map[string]interface{})
				if len(crpatch) > 1 {
					err := errors.New("CR key has multiple maps...")
					log.Error(err, "Failed to update the configmap")
					return ctrl.Result{}, err
				}
				var key string
				var kval interface{}
				for k, val := range crpatch {
					key = k
					kval = val
				}
				if val, ok := cfgMap.Data[key]; !ok {
					cfgMap.Data[key] = patch.Value
					update = true
				} else {
					if strings.ContainsAny(val, "\n | \r") {
						if cfgdata, err := r.convert_mlstring(val); err != nil {
							log.Error(err, "Cannot convert multiline cfgmap")
							return ctrl.Result{}, err
						} else {
							omarshal, _ := yaml.Marshal(cfgdata)
							nl, _ := r.convert_mlstring(patch.Value)
							if cfg, upd := r.multiline_update(kval, cfgdata, nl, deleteInProgress); upd == true {
								marshal, _ := yaml.Marshal(cfg)
								cfgMap.Data[key] = string(marshal)
								update = true
								cfgCr.Status.OldValues = append(cfgCr.Status.OldValues, kncck8sappconfigcomv1alpha1.CfgPatch{Key: key, Value: string(omarshal)})
							}

						}
					}
				}
			case reflect.String:
				k := reflect.ValueOf(crdata).String()
				if oldval, ok := cfgMap.Data[k]; !ok {
					if deleteInProgress == false {
						cfgMap.Data[k] = patch.Value
						cfgCr.Status.OldValues = append(cfgCr.Status.OldValues, kncck8sappconfigcomv1alpha1.CfgPatch{Key: k, Value: ""})
						update = true
					}

				} else {
					if deleteInProgress == false {
						if cfgMap.Data[k] != patch.Value {
							cfgMap.Data[k] = patch.Value
							cfgCr.Status.OldValues = append(cfgCr.Status.OldValues, kncck8sappconfigcomv1alpha1.CfgPatch{Key: k, Value: oldval})
							update = true
						}
					} else {
						delete(cfgMap.Data, k)
						update = true
					}
				}

			}
		}
	}
	if cfgCr.ObjectMeta.DeletionTimestamp.IsZero() {
		if update == true {
			if cfgerr := r.Client.Update(ctx, cfgMap); cfgerr != nil {
				log.Error(cfgerr, "Failed to update configmap")
				cfgCr.Status.Result = "Failed"
				if err := r.Status().Update(ctx, &cfgCr); err != nil {
					log.Error(err, "Failed to upate config CR status")
					return ctrl.Result{}, err
				}
				return ctrl.Result{RequeueAfter: 30 * time.Second}, cfgerr
			} else {
				cfgCr.Status.Result = "Success"
				if err := r.Status().Update(ctx, &cfgCr); err != nil {
					log.Error(err, "Failed to upate config CR status")
					return ctrl.Result{}, err
				}
				return ctrl.Result{}, nil
			}
		}

	} else {
		if err := r.reconcileDelete(ctx, &cfgCr, cfgMap, update); err != nil {
			log.Error(err, "Failed during deletion")
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil

}

// SetupWithManager sets up the controller with the Manager.
func (r *ConfigCtrlReconciler) SetupWithManager(mgr ctrl.Manager) error {

	ps := builder.WithPredicates(predicate.GenerationChangedPredicate{})
	return ctrl.NewControllerManagedBy(mgr).
		For(&kncck8sappconfigcomv1alpha1.ConfigCtrl{}, ps).
		Complete(r)
}
