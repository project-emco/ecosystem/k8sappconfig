```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2021 Intel Corporation
```

# Kubernetes Native Configuration Controller (KNCC) Document

The container image is available in the registry: **registry.gitlab.com/project-emco/ecosystem/k8sappconfig/cfgcontroller:latest**

# Steps to build the KNCC:

1. To create the KNCC container image, run the following command (in the src folder)

```
make docker-build

```
2. The deployment files are available in the deploy folder. It can also be created using the following command

```
make deployment-file

```

This will create the file "cfgcontrol.yaml".

# Steps to deploy the KNCC:

The deployment file "cfgcontrol.yaml" is available in the deploy folder. Run the following command in the kubernetes control plane node to deploy the KNCC,

```
kubectl apply -f <path-to-deployment-file>
```
Example:
```
cd deploy
kubectl apply -f cfgcontrol.yaml

```
