/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.
type HttpHeader struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type NsName struct {
	NameSpace string `json:"ns"`
	Name      string `json:"name"`
}

type HttpTls struct {
	ClientCertificate NsName `json:"clientCertificate,omitempty"`
	ServerTrustCA     NsName `json:"serverTrustCA,omitempty"`
}

type HttpTarget struct {
	// +kubebuilder:validation:Enum=svc;pods
	Type string `json:"type,omitempty"`
	Svc  NsName `json:"svc,omitempty"`
}

type Url struct {
	FullPath string `json:"fullPath,omitempty"`
	Scheme   string `json:"scheme,omitempty"`
	Host     string `json:"host,omitempty"`
	Port     int    `json:"port,omitempty"`
	Path     string `json:"path,omitempty"`
}

type HttpStatus struct {
	EndPoint string       `json:"ep"`
	Code     int          `json:"code,omitempty"`
	Reason   string       `json:"reason,omitempty"`
	Attempts int          `json:"attempts,omitempty"`
	Headers  []HttpHeader `json:"headers,omitempty"`
	Content  string       `json:"content,omitempty"`
}

// RestCrSpec defines the desired state of RestCr
type RestCrSpec struct {
	URL    Url        `json:"url"`
	Target HttpTarget `json:"target,omitempty"`
	// +kubebuilder:validation:Enum=HTTP1_1;HTTP2_0
	Version  string       `json:"version"`
	Headers  []HttpHeader `json:"headers,omitempty"`
	Contents []string     `json:"contents,omitempty"`
	Tls      HttpTls      `json:"tls,omitempty"`
}

// RestCrStatus defines the observed state of RestCr
type RestCrStatus struct {
	NumEP     int                   `json:"numEP"`
	CommonErr string                `json:"commonErr,omitempty"`
	Response  map[string]HttpStatus `json:"response,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:scope=Namespaced

// RestCr is the Schema for the restcrs API
type RestCr struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RestCrSpec   `json:"spec,omitempty"`
	Status RestCrStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// RestCrList contains a list of RestCr
type RestCrList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RestCr `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RestCr{}, &RestCrList{})
}
