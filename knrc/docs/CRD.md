```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2022 Intel Corporation
```

# KNRC Custom Resouce Definition:

The Custom Resource Defintion for the KNRC is as below,

```
apiVersion: lfn.edge-5g.com/v1alpha1
Kind:  restcr
metadata:
    name:  <name>
    namespace: <namespace>
 spec:
     url:
         fullPath:  <string>
     target:
          type:  <svc | pods>
          epID:
             ns: <namespace>
             name: <svc-name>
     version:  <string: HTTP/1.1, HTTP/2>
     tls:
        clientCertificate:
           ns:  <secret namespace>
           name:
     headers:
         - name: <string>
            value: <string>
         - name: <string>
            value: <string>
     contents:  
          - <string>
status:
  Request-path:
    attempts: <int>
    code:  <int>
    reason:  <string>
    headers:
       - name: <string>
         value: <string>
    content:  <hex encoded string> 
```

* If the target type is set to pods, then the http request is sent to each of the endpoints that are part of the target service.

* If the target type is set to svc, then the request is sent to the service IP and not to each of the endpoints.

* The status is updated for each endpoint.

* The CA certificate is provided using the secret resource.

