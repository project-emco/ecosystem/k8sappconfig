```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2022 Intel Corporation
```

# Kubernetes Native REST Controller (KNRC):

* The Controller provides support for sending http requests and receive the responses in the kubernetes native way, by using the Custom Resources.

* Custom Resource Definitions (CRDs) provide the ability to define the http request operation. The received responses are stored in the status fields.

* The controller is built using the golang net/http client package.

# Supported Features:

* Retry the failed http request operations with variable backoff period between the retries.

* Watch the endpoints and retry the request when they are restarted.

* Ability to send the request to each endpoint that is part of the service, when configured.

* support for both http and https. User can provide the certificates via secret.

* Stores the response in the status fields.

* kubectl commands (get, delete, update) are translated to the corresponding http commands.

# Advantages:
Advantages of KNRC:

* Support for existing CNFs/Apps.
* No need for separate persistent storage by CNFs/APPs for configuration.
* Auto configures scaled-out instances and restarted instances.
* Memory savings (One CRD controller for all).
* Works smoothly with Multi Cluster Orchestrators such as EMCO

# Documentation:

* Refer to the docs folder for the CRD documentation.

