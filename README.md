```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2022 Intel Corporation
```

# Kubernetes Application Configuration

This repo has the kubernetes controllers that can be used for configuring the applicaitons or services running in a cluster.
It contains the following controllers,

* KNRC : Kubernetes Native REST Controller
* KNCC : Kubernetes Native Configuration Controller



