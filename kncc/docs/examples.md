```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2022 Intel Corporation
```

# KNCC examples:

## Example 1
* The initial configmap,

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: cert-example
  namespace: default
data:
  mesh: |
    caCertificates:
    - certSigners:
      - clusterissuers.manager.io/system
      pem: |
        -----BEGIN CERTIFICATE-----
        xxxxxxxxxxx
        -----END CERTIFICATE-----
    - certSigners:
      - clusterissuers.manager.io/foo
      pem: |
        -----BEGIN CERTIFICATE-----
        ^^^^^^^^^^^^^^^^^
        -----END CERTIFICATE-----
  key1: val1

```

* The KNCC CR to modify the above configmap is as below,

```
apiVersion: kncc.k8sappconfig.com.gitlab.com/v1alpha1
kind: ConfigCtrl
metadata:
  name: cert-cr
  namespace: default
spec:
  resource:
    name: cert-example
    namespace: default
  patch:
    - key: |
        mesh:
          caCertificates
      value: |
        - certSigners:
          - clusterissuers.manager.io/bar
          pem: |
            -----BEGIN CERTIFICATE-----
            ===============
            -----END CERTIFICATE-----
    - key: key1
      value: value2
    - key: new
      value: val3
```

* When the above configmap and KNCC CR are applied to the cluster, the configmap is modified to the following,

```
apiVersion: v1
data:
  key1: value2
  mesh: |
    caCertificates:
        - certSigners:
            - clusterissuers.manager.io/system
          pem: |
            -----BEGIN CERTIFICATE-----
            xxxxxxxxxxx
            -----END CERTIFICATE-----
        - certSigners:
            - clusterissuers.manager.io/foo
          pem: |
            -----BEGIN CERTIFICATE-----
            ^^^^^^^^^^^^^^^^^
            -----END CERTIFICATE-----
        - certSigners:
            - clusterissuers.manager.io/bar
          pem: |
            -----BEGIN CERTIFICATE-----
            ===============
            -----END CERTIFICATE-----
  new: val3
kind: ConfigMap
metadata:
  creationTimestamp: "2022-06-06T13:46:23Z"
  name: cert-example
  namespace: default
  resourceVersion: "2986681"
```

## Example 2
* The initial configmap,

```
apiVersion: v1
data:
  key1: val1
  key2: |-
    - subval1
    - subval2
  key3: |-
    Hai How are you
    Fine, Thank you
    Bye
kind: ConfigMap
metadata:
  name: cfg-test
  namespace: default

```

* The KNCC CR to modify the above configmap is as below,

```
apiVersion: kncc.k8sappconfig.com.gitlab.com/v1alpha1
kind: ConfigCtrl
metadata:
  name: cfgcr-test
  namespace: default
spec:
  resource:
    name: cfg-test
    namespace: default
  patch:
    - key: cfgkey1
      value: cfgval1
    - key: cfgkey2
      value: cfgval2
    - key: key1
      value: val2
    - key: |
        kkkkkk
      value: |
        - subval3

```

* When the above configmap and KNCC CR are applied to the cluster, the configmap is modified to the following,

```
apiVersion: v1
data:
  cfgkey1: cfgval1
  cfgkey2: cfgval2
  key1: val2
  key2: |-
    - subval1
    - subval2
  key3: |-
    Hai How are you
    Fine, Thank you
    Bye
  kkkkkk: |
    - subval3
kind: ConfigMap
metadata:
  name: cfg-test
  namespace: default

```

## Example 3
* The initial configmap,

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: istio-crt
  namespace: default
data:
  mesh: |
    accessLogEncoding: JSON
    accessLogFile: /dev/stdout
    caCertificates:
      - certSigners:
        - clusterissuers.cert-manager.io/bar
        pem: ""
    defaultConfig:
      discoveryAddress: istiod.istio-system.svc:15012
      proxyMetadata:
        ISTIO_META_CERT_SIGNER: istio-system
      tracing:
        zipkin:
          address: zipkin.istio-system:9411
    enablePrometheusMerge: true
    rootNamespace: istio-system
    trustDomain: cluster.local
  meshNetworks: 'networks: {}'

```

* The KNCC CR to modify the above configmap is as below,

```
apiVersion: kncc.k8sappconfig.com.gitlab.com/v1alpha1
kind: ConfigCtrl
metadata:
  name: istio-crt
  namespace: default
spec:
  resource:
    name: istio-crt
    namespace: default
  patch:
    - key: |
        mesh:
         defaultConfig:
           proxyMetadata:
             ISTIO_META_CERT_SIGNER
      value: foo-istio-system
    - key: |
        mesh:
         defaultConfig:
           proxyMetadata:
             BAR_META_CERT_SIGNER
      value: bar-system
    - key: key1
      value: value2
    - key: new
      value: val3

```

* When the above configmap and KNCC CR are applied to the cluster, the configmap is modified to the following,

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: istio-crt
  namespace: default
data:
  key1: value2
  mesh: |
    accessLogEncoding: JSON
    accessLogFile: /dev/stdout
    caCertificates:
      - certSigners:
        - clusterissuers.cert-manager.io/bar
        pem: ""
    defaultConfig:
      discoveryAddress: istiod.istio-system.svc:15012
      proxyMetadata:
          BAR_META_CERT_SIGNER: bar-system
          ISTIO_META_CERT_SIGNER: foo-istio-system
      tracing:
        zipkin:
          address: zipkin.istio-system:9411
    enablePrometheusMerge: true
    rootNamespace: istio-system
    trustDomain: cluster.local
  meshNetworks: 'networks: {}'
  new: val3

```
