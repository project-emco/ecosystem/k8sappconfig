```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2022 Intel Corporation
```

# KNCC Custom Resouce Definition:

The Custom Resource Defintion for the KNCC is as below,

```
apiVersion: kncc.k8sappconfig.com.gitlab.com/v1alpha1
kind: ConfigCtrl
metadata:
  name: <cr-name>
  namespace: <cr-namespace>
spec:
  resource:
    name: <configmap-name>
  patch:
    - key: <cfgkey1>
      value: <cfgval1>
    - key: <cfgkey2>
      value: <fgval2>
    - key: |
        <mltiline-keys>
      value: |
        <multiline-values>

```

* The configmap resource must be in the same namespace as the CR.

* Multiple Key Value pairs can be provided in the CR.

* Multi-Line keys and Values can be provided. KNCC will try to match the keys and values inside the multi-line strings.
Please refer to the examples.


