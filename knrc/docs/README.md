```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2021 Intel Corporation
```

# Kubernetes Native REST Controller (KNRC) Document

The container image is available in the registry: **registry.gitlab.com/project-emco/ecosystem/k8sappconfig/restcontroller:latest**

# Steps to build the KNRC:

1. To create the KNRC container image, run the following command (in the src folder)

```
make docker-build

```
2. The deployment files are available in the deploy folder. It can also be created using the following command

```
make deployment-file

```

This will create the file "restcontrol.yaml".

# Steps to deploy the KNRC:

The deployment file "restcontrol.yaml" is available in the deploy folder. Run the following command in the kubernetes control plane node to deploy the KNRC,

```
kubectl apply -f <path-to-deployment-file>
```
Example:
```
cd deploy
kubectl apply -f restcontrol.yaml

```
