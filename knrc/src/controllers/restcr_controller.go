/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"time"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	lfnedge5gcomv1alpha1 "github.com/akraino-edge-stack/icn-edge-5g/api/v1alpha1"
	"github.com/go-logr/logr"
)

// RestCrReconciler reconciles a RestCr object
type RestCrReconciler struct {
	client.Client
	Log      logr.Logger
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
	// Saving the state. Need to remove this.
	RestClientMap map[string]*http.Client
}

var (
	testLocal = false
	// Default retry configuration
	defaultRetryWaitMin = 1 * time.Second
	defaultRetryWaitMax = 60 * time.Second
	respReadLimit       = int64(4096)

	// A regular expression to match the error returned by net/http when the
	// configured number of redirects is exhausted.
	redirectsErrorRe = regexp.MustCompile(`stopped after \d+ redirects\z`)

	// A regular expression to match the error returned by net/http when the
	// scheme specified in the URL is invalid. This error isn't typed
	// specifically so we resort to matching on the error string.
	schemeErrorRe = regexp.MustCompile(`unsupported protocol scheme`)
)

func RestRetryPolicy(ctx context.Context, resp *http.Response, err error) (bool, error) {
	if ctx.Err() != nil {
		return false, ctx.Err()
	}

	if err != nil {
		if v, ok := err.(*url.Error); ok {
			// Don't retry if the error was due to too many redirects.
			if redirectsErrorRe.MatchString(v.Error()) {
				return false, v
			}

			// Don't retry if the error was due to an invalid protocol scheme.
			if schemeErrorRe.MatchString(v.Error()) {
				return false, v
			}

			// Don't retry if the error was due to TLS cert verification failure.
			if _, ok := v.Err.(x509.UnknownAuthorityError); ok {
				return false, v
			}
		}

		// The error is likely recoverable so retry.
		return true, nil
	}

	if resp.StatusCode == http.StatusTooManyRequests {
		return true, nil
	}

	if resp.StatusCode == 0 || (resp.StatusCode >= 500 && resp.StatusCode != http.StatusNotImplemented) {
		return true, fmt.Errorf("unexpected HTTP status %s", resp.Status)
	}

	return false, nil
}

// RestBackoff : Calculates the backoff period based on number of attempts, before retrying the http request.
func RestBackoff(min, max time.Duration, attemptNum int, resp *http.Response) time.Duration {
	if resp != nil {
		if resp.StatusCode == http.StatusTooManyRequests || resp.StatusCode == http.StatusServiceUnavailable {
			if s, ok := resp.Header["Retry-After"]; ok {
				if sleep, err := strconv.ParseInt(s[0], 10, 64); err == nil {
					return time.Second * time.Duration(sleep)
				}
			}
		}
	}

	mult := math.Pow(2, float64(attemptNum)) * float64(min)
	sleep := time.Duration(mult)
	if float64(sleep) != mult || sleep > max {
		sleep = max
	}
	return sleep
}

func drainBody(body io.ReadCloser) error {
	defer body.Close()
	_, err := io.Copy(ioutil.Discard, io.LimitReader(body, respReadLimit))
	return err
}

func getDataFromSecret(r client.Client, ns string, name string, key string) ([]byte, error) {
	instance := &corev1.Secret{}
	err := r.Get(context.Background(), client.ObjectKey{
		Namespace: ns,
		Name:      name,
	}, instance)
	if err != nil {
		return []byte{}, err
	}
	return instance.Data[key], nil
}

// getCACertificates : Extracts the CA certficate from the secret.
func (r *RestCrReconciler) getCACertificates(clnt client.Client, restcr *lfnedge5gcomv1alpha1.RestCr) (*x509.CertPool, error) {
	caCertPool := x509.NewCertPool()
	if (restcr.Spec.Tls != lfnedge5gcomv1alpha1.HttpTls{}) && (restcr.Spec.Tls.ServerTrustCA != lfnedge5gcomv1alpha1.NsName{}) {
		cacert, err := getDataFromSecret(clnt, restcr.Spec.Tls.ServerTrustCA.NameSpace, restcr.Spec.Tls.ServerTrustCA.Name, "ca.crt")
		if err != nil {
			r.Log.Error(err, "Failed to get the secret")
			return nil, err
		}
		ok := caCertPool.AppendCertsFromPEM(cacert)
		if !ok {
			err := errors.New("CA certificate pool Error")
			r.Log.Error(err, "Failed to get certificate")
			return nil, err
		}
	}
	return caCertPool, nil
}

// getEPPaths: Generates the URL path for each endpoint in a svc.
func (r *RestCrReconciler) getEPPaths(restcr *lfnedge5gcomv1alpha1.RestCr, ep *corev1.Endpoints) (int, []string, error) {
	var fPath *url.URL
	var allEPs []string
	eps := 0
	if restcr.Spec.URL.FullPath != "" {
		var err error
		if fPath, err = url.Parse(restcr.Spec.URL.FullPath); err != nil {
			r.Log.Error(err, "cannot parse the url")
			return eps, allEPs, err
		}
	}
	for _, subset := range ep.Subsets {
		eps = eps + len(subset.Addresses)
		for _, addr := range subset.Addresses {
			_, port, _ := net.SplitHostPort(fPath.Host)
			if fPath.Port() == "" {
				fPath.Host = addr.IP
			} else {
				fPath.Host = addr.IP + ":" + port
			}
			epPath := fPath.String()
			allEPs = append(allEPs, epPath)
			if _, ok := restcr.Status.Response[epPath]; !ok {
				restcr.Status.Response[epPath] = lfnedge5gcomv1alpha1.HttpStatus{EndPoint: "ep"}
			}

		}

	}
	return eps, allEPs, nil
}

// httpCreateRequest - Creates and sends the http request.
// Takes an array of URLs (each to the svc or the endpoints) and sends the request to each of them.
func (r *RestCrReconciler) httpCreateRequest(ctx context.Context, restReq *lfnedge5gcomv1alpha1.RestCr, httpClient *http.Client, method string, upath []string) (ctrl.Result, error, bool) {
	var resp *http.Response
	var shouldRetry bool
	var doErr, checkErr error
	var req *http.Request
	var err error
	var verb string

	switch method {
	case http.MethodPut:
		verb = "Create"
	case http.MethodDelete:
		verb = "Delete"
	default:
		r.Log.V(1).Info("Unknown", "method", method)
		return ctrl.Result{}, errors.New("Unknown method"), false
	}
	retry := false
	var waitTime time.Duration
	numSuccess := 0
	for _, path := range upath {
		httpStat := restReq.Status.Response[path]

		dec, _ := base64.StdEncoding.DecodeString(restReq.Spec.Contents[0])
		req, err = http.NewRequest(method, path, bytes.NewBuffer(dec))
		if err != nil {
			r.Log.Error(err, "Error in GET NewRequest...")
			return ctrl.Result{}, client.IgnoreNotFound(err), false
		}
		for _, head := range restReq.Spec.Headers {
			req.Header.Set(head.Name, head.Value)
		}
		resp, doErr = httpClient.Do(req)
		shouldRetry, checkErr = RestRetryPolicy(req.Context(), resp, doErr)
		httpStat.Attempts = httpStat.Attempts + 1
		defer httpClient.CloseIdleConnections()
		if !shouldRetry {
			if doErr == nil && checkErr == nil {
				defer resp.Body.Close()
				bodyBytes, err := io.ReadAll(resp.Body)
				if err != nil {
					r.Log.Error(err, "Failed to read the response body")
					httpStat.Reason = verb + "Failed"
				} else {
					bodyString := string(bodyBytes)
					r.Log.V(1).Info(bodyString)
					httpStat.Code = resp.StatusCode
					httpStat.Reason = verb + "Success"
					for key, arrval := range resp.Header {
						for _, val := range arrval {
							httpStat.Headers = append(httpStat.Headers, lfnedge5gcomv1alpha1.HttpHeader{Name: key, Value: val})
						}
					}
					httpStat.Content = bodyString
					numSuccess = numSuccess + 1
				}
			} else {
				//defer httpClient.CloseIdleConnections()
				if resp != nil {
					httpStat.Code = resp.StatusCode
					drainBody(resp.Body)
				}
				httpStat.Reason = verb + "Failed"
			}
		} else {
			httpStat.Reason = verb + "Retrying"
			if doErr == nil {
				drainBody(resp.Body)
			}
			wait := RestBackoff(defaultRetryWaitMin, defaultRetryWaitMax, httpStat.Attempts, resp)
			retry = true
			if waitTime == 0 {
				waitTime = wait
			} else if wait < waitTime {
				waitTime = wait
			}

			r.Log.V(1).Info("Retryng the request after", "seconds", wait.String())
		}
		restReq.Status.Response[path] = httpStat
	}
	if retry {
		return ctrl.Result{RequeueAfter: waitTime}, nil, true
	} else {
		if numSuccess == len(upath) {
			return ctrl.Result{}, nil, false
		} else {
			return ctrl.Result{}, errors.New("Certain requests failed"), false
		}
	}
}

//+kubebuilder:rbac:groups=lfn.edge-5g.com.github.com,resources=restcrs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=lfn.edge-5g.com.github.com,resources=restcrs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=lfn.edge-5g.com.github.com,resources=restcrs/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the RestCr object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile
func (r *RestCrReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("RestCRService", req.NamespacedName)
	var restReq lfnedge5gcomv1alpha1.RestCr

	log.V(1).Info("Entering Reconcile....")
	if err := r.Get(ctx, req.NamespacedName, &restReq); err != nil {
		log.Error(err, "unable to fetch rest CR")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	//Intialize the status response
	if restReq.Status.Response == nil {
		restReq.Status.Response = make(map[string]lfnedge5gcomv1alpha1.HttpStatus)
	}

	nep := 0
	var allEPs []string
	if restReq.Spec.Target.Type == "svc" && (restReq.Spec.Target.Svc.Name == "" || restReq.Spec.Target.Svc.NameSpace == "") {
		nep = 1
		svcPath := restReq.Spec.URL.FullPath
		allEPs = append(allEPs, svcPath)
		if _, ok := restReq.Status.Response[svcPath]; !ok {
			restReq.Status.Response[svcPath] = lfnedge5gcomv1alpha1.HttpStatus{EndPoint: "svc"}
		}
	} else {

		if restReq.Spec.Target.Svc.Name == "" || restReq.Spec.Target.Svc.NameSpace == "" {
			log.Error(errors.New("Missing Target Service spec"), "cr: ", restReq.Name)
			return ctrl.Result{}, errors.New("Missing Target Service spec")
		}

		ep := &corev1.Endpoints{}
		if err := r.Client.Get(ctx, types.NamespacedName{Namespace: restReq.Spec.Target.Svc.NameSpace, Name: restReq.Spec.Target.Svc.Name}, ep); err != nil {
			log.Error(err, "Unable to fetch the endpoint")
		}
		// Retry the svc request when the endpoints change.
		if restReq.Spec.Target.Type == "svc" {
			nep = 1
			svcPath := restReq.Spec.URL.FullPath
			allEPs = append(allEPs, svcPath)
			if _, ok := restReq.Status.Response[svcPath]; !ok {
				restReq.Status.Response[svcPath] = lfnedge5gcomv1alpha1.HttpStatus{EndPoint: "svc"}
			} else if restReq.Status.Response[svcPath].Reason != "CreateRetrying" {
				httpStat := restReq.Status.Response[svcPath]
				httpStat.Reason = "CreateRetrying"
				httpStat.Headers = []lfnedge5gcomv1alpha1.HttpHeader{}
				httpStat.Content = ""
				restReq.Status.Response[svcPath] = httpStat
			}
		} else if restReq.Spec.Target.Type == "pods" {
			var err error
			nep, allEPs, err = r.getEPPaths(&restReq, ep)
			if err != nil {
				log.Error(err, "cannot parse the url")
				return ctrl.Result{}, err
			}
		}
	}
	// Remove the obsolete responses.
	for k, _ := range restReq.Status.Response {
		keyMatch := false
		for _, uk := range allEPs {
			if k == uk {
				keyMatch = true
				break
			}
		}
		if keyMatch == false {
			log.Info("Removing the key from response : " + k)
			delete(restReq.Status.Response, k)
		}
	}

	restReq.Status.NumEP = nep

	var httpClient *http.Client
	if restClient, ok := r.RestClientMap[req.NamespacedName.String()]; ok {
		// Client already exists. we can retry the request now
		httpClient = restClient
	} else {
		caCertPool, caCertErr := r.getCACertificates(r.Client, &restReq)
		if caCertErr != nil {
			restReq.Status.CommonErr = "CA Cert Error"
			log.V(1).Info("CA cert Error")
			if err := r.Status().Update(ctx, &restReq); err != nil {
				log.Error(err, "failed to update the status CA error")
				return ctrl.Result{}, err
			}
			return ctrl.Result{}, caCertErr
		}
		// Create the client and try
		httpClient = &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				TLSClientConfig: &tls.Config{
					RootCAs: caCertPool,
				},
			},
		}
		r.RestClientMap[req.NamespacedName.String()] = httpClient
	}

	// name of our custom finalizer
	myFinalizerName := "restcr.lfn.edge-5g.com/finalizer"

	// examine DeletionTimestamp to determine if object is under deletion
	if restReq.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,
		// then lets add the finalizer and update the object. This is equivalent
		// registering our finalizer.
		if !controllerutil.ContainsFinalizer(&restReq, myFinalizerName) {
			controllerutil.AddFinalizer(&restReq, myFinalizerName)
			if err := r.Update(ctx, &restReq); err != nil {
				log.Error(err, "failed to update the finalizer")
				return ctrl.Result{}, err
			} else {
				return ctrl.Result{}, nil
			}
		}
		var result ctrl.Result
		var retErr error
		var retry bool
		var pendEPs []string

		for _, path := range allEPs {
			if restReq.Status.Response[path].Reason != "CreateSuccess" {
				pendEPs = append(pendEPs, path)
			}
		}
		if len(pendEPs) > 0 {
			result, retErr, retry = r.httpCreateRequest(ctx, &restReq, httpClient, http.MethodPut, pendEPs)

		} else {
			result, retErr, retry = ctrl.Result{}, error(nil), false
		}
		if err := r.Status().Update(ctx, &restReq); err != nil {
			log.Error(err, "failed to update the status")
			return ctrl.Result{}, err
		}
		if retry == false {
			delete(r.RestClientMap, req.NamespacedName.String())
		}
		return result, retErr

	} else {
		// The object is being deleted
		if controllerutil.ContainsFinalizer(&restReq, myFinalizerName) {
			var result ctrl.Result
			var retErr error
			var retry bool
			var delEPs []string
			// our finalizer is present, so lets handle any external dependency
			for _, path := range allEPs {
				if restReq.Status.Response[path].Reason == "CreateSuccess" {
					delEPs = append(delEPs, path)
				}
			}
			if len(delEPs) > 0 {
				result, retErr, retry = r.httpCreateRequest(ctx, &restReq, httpClient, http.MethodDelete, delEPs)
			} else {
				result, retErr, retry = ctrl.Result{}, error(nil), false
			}
			if retry == false {
				delete(r.RestClientMap, req.NamespacedName.String())
				// remove our finalizer from the list and update it.
				controllerutil.RemoveFinalizer(&restReq, myFinalizerName)
				if err := r.Update(ctx, &restReq); err != nil {
					return ctrl.Result{}, err
				}
			}
			return result, retErr
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *RestCrReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.RestClientMap = map[string]*http.Client{}
	ps := builder.WithPredicates(predicate.GenerationChangedPredicate{})

	// Create the Index
	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &lfnedge5gcomv1alpha1.RestCr{}, "spec.target.svc.name", func(rawObj client.Object) []string {
		restCR := rawObj.(*lfnedge5gcomv1alpha1.RestCr)
		return []string{restCR.Spec.Target.Svc.Name}
	}); err != nil {
		return err
	}

	// List the needed CR to specific events and return the reconcile Requests
	mapFn := func(obj client.Object) []reconcile.Request {
		var enqueueRequest []reconcile.Request
		ep := obj.(*corev1.Endpoints)
		epNs := ep.Namespace
		epName := ep.Name
		var crliststruct lfnedge5gcomv1alpha1.RestCrList
		ctx := context.Background()
		err := r.List(ctx, &crliststruct, client.InNamespace(epNs), client.MatchingFields{"spec.target.svc.name": epName})
		if err != nil {
			r.Log.Error(err, "cannot list the cr for ep")
			return enqueueRequest
		}
		for _, rc := range crliststruct.Items {
			req := reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      rc.Name,
					Namespace: rc.Namespace,
				}}
			enqueueRequest = append(enqueueRequest, req)
			r.Log.Info("enq req", "enq: ", req.String())

		}
		return enqueueRequest
	}

	// Watch for the restcr and endpoints
	return ctrl.NewControllerManagedBy(mgr).
		For(&lfnedge5gcomv1alpha1.RestCr{}, ps).
		Watches(&source.Kind{Type: &corev1.Endpoints{}}, handler.EnqueueRequestsFromMapFunc(mapFn)).
		Complete(r)
}
